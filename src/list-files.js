const { google } = require("googleapis");
const credentials = require("../res/credentials.json");

// Scopes required for the Google Drive API, see https://developers.google.com/drive/api/v3/about-auth
const scopes = ["https://www.googleapis.com/auth/drive.file"];

// Create a new JWT client using the key file downloaded from the Google Developer Console
const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);

// Drive service initialization
const drive = google.drive({
  version: "v3",
  auth,
});

// Lists allfiles within the drive
drive.files.list(
  {
    supportsAllDrives: true, // This is required to access Shared drives
    includeItemsFromAllDrives: true, // Include files from shared drives
    corpora: "allDrives", // Include files from all drives (shared and personal)
  },
  (err, res) => {
    if (err) throw err;
    const files = res.data.files;
    if (files.length) {
      files.map(file => {
        console.log(file);
      });
    } else {
      console.log("No files found");
    }
  }
);
