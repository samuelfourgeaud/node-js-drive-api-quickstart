const { google } = require("googleapis");
const credentials = require("../res/credentials.json");
const fs = require("fs");

// Scopes required for the Google Drive API, see https://developers.google.com/drive/api/v3/about-auth
const scopes = ["https://www.googleapis.com/auth/drive"];

// Create a new JWT client using the key file downloaded from the Google Developer Console
const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);

// Drive service initialization
const drive = google.drive({
  version: "v3",
  auth,
});

// Destination folder ID
const UPLOAD_FOLDER = "1Z85Sk.......vSaHeHy9j";
// Name of the uploaded file
const DEST_NAME = "file-uploaded.txt";
// Path of the file to upload
const FILE_PATH = "./res/file-to-upload.txt";

// Uploaded file metadata
var fileMetadata = {
  name: DEST_NAME,
  parents: [UPLOAD_FOLDER],
};
var media = {
  mimeType: "text/plain",
  body: fs.createReadStream(FILE_PATH),
};

// API call to upload file
drive.files
  .create({
    resource: fileMetadata,
    media: media,
    fields: "id",
    supportsAllDrives: true,
    includeItemsFromAllDrives: true,
  })
  .then(res =>
    console.log(
      `File uploaded to https://drive.google.com/file/d/${res.data.id}`
    )
  );
