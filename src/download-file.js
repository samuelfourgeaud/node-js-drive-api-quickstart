const { google } = require("googleapis");
const credentials = require("../res/credentials.json");
const fs = require("fs");
const path = require("path");
const os = require("os");
const uuid = require("uuid");

const scopes = ["https://www.googleapis.com/auth/drive.file"];

// Create a new JWT client using the key file downloaded from the Google Developer Console
const auth = new google.auth.JWT(
  credentials.client_email,
  null,
  credentials.private_key,
  scopes
);

//  Drive service initialization
const drive = google.drive({
  version: "v3",
  auth,
});

// ID of the file to download
const FILE_ID = "11SmgU............FFG7";

// File destination path
const DEST_NAME = "photo.png";

// Create a new local file stream
const dest = fs.createWriteStream(DEST_NAME);

// API call to download file
drive.files
  .get({ fileId: FILE_ID, alt: "media" }, { responseType: "stream" })
  .then(res => {
    const filePath = path.join(os.tmpdir(), uuid.v4());
    console.log(`writing to ${filePath}`);
    let progress = 0;

    res.data
      .on("end", () => {
        console.log("\nDone downloading file.");
      })
      .on("error", err => {
        console.error("Error downloading file.");
        reject(err);
      })
      .on("data", d => {
        progress += d.length;
        if (process.stdout.isTTY) {
          process.stdout.clearLine();
          process.stdout.cursorTo(0);
          process.stdout.write(`Downloaded ${progress} bytes`);
        }
      })
      .pipe(dest);
  });
