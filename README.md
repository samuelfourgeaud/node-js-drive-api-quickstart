# What is this?

This sample allows to list/download/upload files to and from a Google Shared Drive using Service account credentials

# How to use this sample?

## Download the credentials

- Go to https://console.cloud.google.com/iam-admin/serviceaccounts to download a json file containing the credentials for the service account.
- Rename the file downloaded to `credentials.json` and move it to `/res`
- Make sure that the Shared Drive access has been shared to the service account's email.

## Run the samples

- To list files from the Shared Drive, use `npm run list`.
- To upload a file to the Shared Drive, open `src/upload-file` and update teh UPLOAD_FOLDER variable to a folder/Shared Drive ID. Use `npm run upload`.
- To download a file from the Shared Drive, open `src/download-file.js` and update the FILE_ID variable. Use `npm run download` to download the file.
